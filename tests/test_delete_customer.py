import allure
import pytest

from pages.customers_page import CustomersPage

@allure.feature('XYZ Bank')
@allure.story('UI')
@allure.title('Удаление клиентов')
@allure.description("""
Цель: Проверить удаление клиентов, у которых длина имени будет ближе к среднему арифметическому

        Предусловие:
        Открыть браузер

        Шаги:
        1. Открыть страницу со списком клиентов
        2. Составить список клиентов на удаление
        3. Удалить клиентов
        4. Проверить корректность удаления

        Ожидаемый результат:
        Клиенты удалены корректно
""")

@pytest.mark.run(order=-1)
def test_delete_customers(browser, get_url_config):

    with allure.step('Открыть страницу со списком клиентов'):
        customers_page = CustomersPage(browser)
        customers_page.open_page(get_url_config)
        customers_page.move_to_customers_page()

    with allure.step('Составить список клиентов на удаление'):
        first_names_list = customers_page.get_customers_first_names()
        new_customers_list, customers_indexes_to_delete = customers_page.find_customers_to_delete(first_names_list)

    with allure.step('Удалить клиентов'):
        for idx in customers_indexes_to_delete:
            customers_page.delete_customer(idx)

    with allure.step('Проверить корректность удаления'):
        updated_first_names_list = customers_page.get_customers_first_names()
        assert updated_first_names_list == new_customers_list
