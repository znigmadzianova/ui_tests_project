import datetime
import json

import allure
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromeService

@pytest.fixture(scope="session")
def browser():
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    browser = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
    browser.maximize_window()
    yield browser
    browser.quit()

@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    result = outcome.get_result()

    if result.failed:
        if 'browser' in item.fixturenames:
            driver = item.funcargs['browser']
            allure.attach(
                driver.get_screenshot_as_png(),
                name=f'screenshot_{datetime.datetime.utcnow()}',
                attachment_type=allure.attachment_type.PNG
            )

@pytest.fixture(scope="session")
def get_url_config():
    url = ''
    with open('data/config.json', 'r') as f:
        configs = json.load(f)
        url = configs['url']
    return url