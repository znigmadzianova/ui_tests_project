import allure
import pytest

from pages.customers_page import CustomersPage


@allure.feature('XYZ Bank')
@allure.story('UI')
@allure.title('Сортировка клиентов')
@allure.description("""
Цель: Проверить сортировку в списке клиентов по полю Имя

        Предусловие:
        Открыть браузер

        Шаги:
        1. Открыть страницу со списком клиентов
        2. Задать сортировку по полю Имя Z-A
        3. Проверить верность сортировки
        4. Задать сортировку по полю Имя A-Z
        5. Проверить верность сортировки

        Ожидаемый результат:
        Сортировка работает корректно
""")

@pytest.mark.run(order=2)
def test_sort_customers_by_first_name(browser, get_url_config):

    with allure.step('Открыть страницу со списком клиентов'):
        customers_page = CustomersPage(browser)
        customers_page.open_page(get_url_config)
        customers_page.move_to_customers_page()

    with allure.step('Задать сортировку по полю Имя Z-A'):
        customers_page.sort_customers_by_first_name()

    with allure.step('Проверить верность сортировки'):
        values_list = customers_page.get_customers_first_names()
        assert customers_page.get_sorting_type_in_first_names(values_list) == 'desc'

    with allure.step('Задать сортировку по полю Имя A-Z'):
        customers_page.sort_customers_by_first_name()

    with allure.step('Проверить верность сортировки'):
        values_list = customers_page.get_customers_first_names()
        assert customers_page.get_sorting_type_in_first_names(values_list) == 'asc'
