import allure
import pytest

from pages.add_customer_page import AddNewCustomerPage


@allure.feature('XYZ Bank')
@allure.story('UI')
@allure.title('Создание нового клиента')
@allure.description("""
Цель: Проверить создание нового клиента

        Предусловие:
        Открыть браузер

        Шаги:
        1. Открыть страницу создания нового клиента
        2. Ввести данные нового клиента
        3. Получить сообщение об успешном создании клиента

        Ожидаемый результат:
        Новый клиент успешно создан
""")

@pytest.mark.run(order=1)
def test_add_new_customer(browser, get_url_config):
    with allure.step('Открыть страницу создания нового клиента'):
        new_customer_page = AddNewCustomerPage(browser)
        new_customer_page.open_page(get_url_config)
        new_customer_page.move_to_add_customer_page()

    with allure.step('Ввести данные нового клиента'):
        new_customer_page.add_new_customer()

    with allure.step('Проверить сообщение об успешном создании клиента'):
        new_customer_page.check_message_is_success()
