## Установка и запуск

1. Склонировать репозиторий
```
git clone git@gitlab.com:znigmadzianova/ui_tests_project.git
```
2. Перейти в директорию проекта
3. Создать виртуальное окружение
```
python3 -m venv selenium_env
```
4. Активировать окружение
```
source selenium_env/bin/activate
```
5. Установка зависимостей
```
pip install -r requirements.txt
```
6. Установить путь поиска модулей
```
export PYTHONPATH=$PYTHONPATH:/lib
```
7. Запустить тест
```
pytest -s -v -n=<number of starts> tests --alluredir=allure-results
```
8. Запустить allure отчет
```
allure serve allure-results
```
