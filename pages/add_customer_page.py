from selenium.webdriver.remote.webelement import WebElement

from .base_page import BasePage
from .locators import AddCustomerPage
from lib.helper import generate_post_code, generate_first_name

class AddNewCustomerPage(BasePage):
    success_message: str = 'Customer added successfully'

    @property
    def add_customer_open_page_button(self) -> WebElement:
        return self.element_is_clickable(*AddCustomerPage.ADD_CUSTOMER_OPEN_PAGE_BUTTON)

    @property
    def success_message(self) -> str:
        return 'Customer added successfully'

    @property
    def first_name_input(self) -> WebElement:
        return self.element_is_visible(*AddCustomerPage.FIRST_NAME_INPUT)

    @property
    def last_name_input(self) -> WebElement:
        return self.element_is_visible(*AddCustomerPage.LAST_NAME_INPUT)

    @property
    def postcode_input(self) -> WebElement:
        return self.element_is_visible(*AddCustomerPage.POSTCODE_INPUT)

    @property
    def add_customer_button(self) -> WebElement:
        return self.element_is_clickable(*AddCustomerPage.ADD_CUSTOMER_BUTTON)

    def move_to_add_customer_page(self):
        self.add_customer_open_page_button.click()

    def add_new_customer(self):
        postcode_value = generate_post_code()
        first_name_value = generate_first_name(postcode_value)
        last_name_value = first_name_value[::-1]
        self.first_name_input.send_keys(first_name_value)
        self.last_name_input.send_keys(last_name_value)
        self.postcode_input.send_keys(postcode_value)
        self.add_customer_button.click()

    def check_message_is_success(self) -> str:
        alert_message = self.browser.switch_to.alert
        alert_message_text = alert_message.text
        alert_message.accept()
        return self.success_message in alert_message_text
