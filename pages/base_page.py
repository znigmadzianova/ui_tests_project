from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement
import json

with open('data/config.json', 'r') as f:
    WAIT_TIMEOUT = json.load(f)['wait_timeout']
class BasePage():

    def __init__(self, browser):
        self.browser = browser

    def is_element_present(self, how, what, timeout: int = WAIT_TIMEOUT) -> bool:
        try:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((how, what)))
            return True
        except NoSuchElementException:
            return False

    def element_is_clickable(self, how, what, timeout: int = WAIT_TIMEOUT) -> WebElement:
        return WebDriverWait(self.browser, timeout).until(EC.element_to_be_clickable((how, what)))

    def element_is_visible(self, how, what, timeout: int = WAIT_TIMEOUT) -> WebElement:
        return WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located((how, what)))

    def open_page(self, link):
        self.browser.get(link)

    def alert_is_present(self, timeout: int = WAIT_TIMEOUT):
        return WebDriverWait(self.browser, timeout).until(EC.alert_is_present())