from selenium.webdriver.remote.webelement import WebElement

from .base_page import BasePage
from .locators import CustomersListPage

class CustomersPage(BasePage):

    @property
    def customers_open_page_button(self) -> WebElement:
        return self.element_is_clickable(*CustomersListPage.CUSTOMERS_OPEN_PAGE_BUTTON)

    @property
    def delete_customer_button(self) -> WebElement:
        return self.element_is_clickable(*CustomersListPage.DELETE_CUSTOMER_BUTTON)

    @property
    def sort_customer_by_first_name_button(self) -> WebElement:
        return self.element_is_clickable(*CustomersListPage.SORT_BY_FIRST_NAME)

    @property
    def sort_customer_by_last_name_button(self) -> WebElement:
        return self.element_is_clickable(*CustomersListPage.SORT_BY_LAST_NAME)

    @property
    def sort_customer_by_postcode_button(self) -> WebElement:
        return self.element_is_clickable(*CustomersListPage.SORT_BY_POSTCODE)

    def move_to_customers_page(self):
        self.customers_open_page_button.click()

    def sort_customers_by_first_name(self):
        self.sort_customer_by_first_name_button.click()

    def sort_customers_by_last_name(self):
        self.sort_customer_by_last_name_button.click()

    def sort_customers_by_postcode_name(self):
        self.sort_customer_by_postcode_button.click()

    def get_customers_first_names(self) -> list:
        customers_table = self.browser.find_elements(*CustomersListPage.CUSTOMER_TABLE_DATA)
        first_names_list = []
        for row in customers_table:
            customer_name = row.text.split()[0]
            first_names_list.append(customer_name)
        return first_names_list

    def get_sorting_type_in_first_names(self, first_names_list) -> str:
        list_sorted_desc = sorted(first_names_list, reverse=True, key=str.casefold)
        list_sorted_asc = sorted(first_names_list, key=str.casefold)

        if first_names_list == list_sorted_desc:
            return 'desc'
        elif first_names_list == list_sorted_asc:
            return 'asc'
        else:
            return 'not defined'

    def find_customers_to_delete(self, first_names_list: list) -> tuple[list, list]:
        difference_list = []
        new_customers_list = []
        average_len = sum( map(len, first_names_list) ) / len(first_names_list)
        for el in first_names_list:
            difference_value = abs(len(el) - average_len)
            difference_list.append(difference_value)
        min_indexes = [int(i) for i, val in enumerate(difference_list) if val == min(difference_list)]
        new_customers_list = [first_names_list[i] for i, val in enumerate(first_names_list) if i not in min_indexes]
        min_indexes = sorted(min_indexes, reverse=True)
        return new_customers_list, min_indexes

    def delete_customer(self, customer_index: int):
        delete_button = self.browser.find_elements(*CustomersListPage.DELETE_CUSTOMER_BUTTON)[customer_index]
        delete_button.click()
