from selenium.webdriver.common.by import By

class AddCustomerPage():
    ADD_CUSTOMER_OPEN_PAGE_BUTTON = (By.XPATH, '//button[contains(@ng-click, "add")]')
    FIRST_NAME_INPUT = (By.XPATH, '//input[@ng-model="fName"]')
    LAST_NAME_INPUT = (By.XPATH, '//input[@ng-model="lName"]')
    POSTCODE_INPUT = (By.XPATH, '//input[@ng-model="postCd"]')
    ADD_CUSTOMER_BUTTON = (By.XPATH, '//button[@type="submit"]')
    SUCCESS_MESSAGE = ()

class CustomersListPage():
    CUSTOMERS_OPEN_PAGE_BUTTON = (By.XPATH, '//button[contains(@ng-click, "show")]')
    CUSTOMER_TABLE_DATA = (By.XPATH, '//tbody/tr')
    DELETE_CUSTOMER_BUTTON = (By.XPATH, '//button[contains(@ng-click, "delete")]')
    SORT_BY_FIRST_NAME = (By.XPATH, '//a[contains(@ng-click,"fName")]')
    SORT_BY_LAST_NAME = (By.XPATH, '//a[contains(@ng-click,"lName")]')
    SORT_BY_POSTCODE = (By.XPATH, '//a[contains(@ng-click,"postCd")]')