import random
import string
from textwrap import wrap

def generate_post_code(postcode_length: int = 10) -> str:
    postcode_value = ''.join(map(str, [random.randint(0, 9) for i in range(postcode_length)]))
    return postcode_value

def generate_first_name(postcode_value: str, division_number: int = 2) -> str:
    first_name_value = ''
    transformed_value = wrap(postcode_value, division_number)
    for el in transformed_value:
        el_transformed = int(el) % 25 - 1
        first_name_value += string.ascii_lowercase[el_transformed]
    return first_name_value

